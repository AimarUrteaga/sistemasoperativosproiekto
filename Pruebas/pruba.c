#include <stdio.h>
//
#include <limits.h>

struct hasiera{
    char hasiera[6];
    char edukia[6];
    char jauzia;
};

unsigned int hexToDecimal (char * hex,int luzera){
    int i;
    unsigned base=1,decimal=0;
    for(i = luzera-1; i >= 0; i--){
        if(hex[i] >= '0' && hex[i] <= '9'){
            decimal += (hex[i] - 48) * base;
            base *= 16;
        }
        else if(hex[i] >= 'A' && hex[i] <= 'F'){
            decimal += (hex[i] - 55) * base;
            base *= 16;
        }
        else{
            return UINT_MAX;
        }
    }
    return decimal;
}

int leerELF(char * nombreArchivo){
    FILE * archivo = fopen(nombreArchivo, "r");
    if (archivo == NULL){
        return -1;
    }
    struct hasiera lerroak[2];
    fread(&lerroak,sizeof(struct hasiera),2,archivo);
    //printf("%.6s\n",lerroak[0].edukia);
    printf("1:\t%u\n",hexToDecimal(lerroak[0].edukia,6));
    printf("2:\t%u\n",hexToDecimal(lerroak[1].edukia,6));
    char besteak[9];
    /*while (besteak!=-1){
        fread(&besteak,sizeof(char)*9,1,archivo);
        printf("%.8s\n",besteak);
        printf("besteak:\t%u\n",hexToDecimal(besteak,8));
    }*/
    while (fread(&besteak,sizeof(char)*9,1,archivo)!=0){
        printf("%.8s\t",besteak);
        printf("besteak:\t%u\n",hexToDecimal(besteak,8));
    }
}

void main(){
    leerELF("prog001.elf");
}