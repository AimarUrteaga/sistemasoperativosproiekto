/**
 * @file clock.c
 * @author Mikel Aristu & Aimar Urteaga
 * @brief Gure kernelaren erlojua definitzen duen programa.
 * @version 0.2
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "globals.h"


struct clk_mutex_datu mutex;

/**
 * @brief Sistemaren erlojua, honek tick-ak sortuko ditu,
 * sistemaren "erritmoa" kontrolaraziz.
 *
 * @return void* 
 */
void* clk(){
	while(1){
		/**
		 * @brief Mutex aldagaia blokeatuko du beste hariei sarbidea ukatuz.
		 */
		pthread_mutex_lock(&mutex.mutex);
		/**
		 * @brief done aldagaia txikiago bada tenporizadore kopurua baino, hau da, tenporizadoreek
		 * ez badute beraien lana bukatu, begiztan jarraituko du. 
		 */
		while(mutex.done < mutex.tenp_kop)
			pthread_cond_wait(&mutex.cond, &mutex.mutex);
	
		//printf("Kaixo ni erlojua naiz.\n");
		/**
		 * @brief Tenporizadoreek haien lana bukatzean, done=0
		 */
		mutex.done = 0;
		/**
		 * @brief cond2 aldagaia “broadcast” moduan bidaliko da tenporizadore guztiei
		 */
		pthread_cond_broadcast(&mutex.cond2);
		/**
		 * @brief mutex aldagaia liberatu.
		 */
		pthread_mutex_unlock(&mutex.mutex);
	}
}