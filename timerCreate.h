/**
 * @file timerCreate.h
 * @author Mikel Aristu & Aimar Urteaga
 * @brief tenporizadoreen header fitxategia
 * @version 0.1
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef __TIMERCREATE_H_
#define __TIMERCREATE_H_

#include <stdio.h>

/**
 * @brief Sisteman tenporizadore bat sortzeko eta martxan 
 * jartzeko funtzioaren deklarazioa.
 */
void *timerCreateSchedulers();
void *timerCreateProcess(void * input);
#endif