/**
 * @file processgenerator.h
 * @author Aimar Urteaga & Mikel Aristu
 * @brief processgenerator-aren header fitxategia.
 * @version 0.1
 * @date 2021-11-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef __PROCESSG_H_
#define __PROCESSG_H_
#include <limits.h>
struct hasiera{
    char hasiera[6];
    char edukia[6];
    char jauzia;
};
struct nodoPrograma{
    struct nodoPrograma* hurrengoa;
    unsigned int zenbakia;
};

struct programa{
    struct hasiera lerroak[2];
    struct nodoPrograma* bestelerroak;
    unsigned int nLista;
};

struct pidbitmap{
    char ocupados [UINT_MAX-1];
    unsigned int pos;
    unsigned short lastsscheduler;
    unsigned int lastProgramNumber;
};

void createProcess(unsigned int prioridad, unsigned int tiempo);

void inicializarProcessGenerator();
#endif