/**
 * @file timercreate.c
 * @author Mikel Aristu & Aimar Urteaga
 * @brief Tenporizadoreak sortzeko programa.
 * @version 0.1
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "globals.h"
#include "timerCreate.h"
#include <stdlib.h>


/**
 * @brief Schedulerraren tenporizadorea, schedulerrak exekutatzeko.
 * 
 * @return void* 
 */
void* timerCreateSchedulers(){
	int kont=0;
	int tcSchedulerMaiztasuna=schedulerMaiztasuna;
	/**
	 * @brief Tenporizadore bat sortzean, tenporizadore kopurua handiagotuko da. (+1)
	 */
	mutex.tenp_kop++;
	/**
	* @brief Mutex aldagaia blokeatuko du beste hariei sarbidea ukatuz.
	*/
	pthread_mutex_lock(&mutex.mutex);
	while(1){
		/**
		 * @brief done aldagai konpartitua +1, honek adieraziko du tenporizadore batek 
		 * behar zuena exekutatu duela.
		 */
		mutex.done++;
		if (kont==tcSchedulerMaiztasuna){
			printf("Kaixo ni schedulerren tenporizadore bat naiz.\n");
			schedulers();
			kont=0;
		}
		kont++;
		/**
		 * @brief cond aldagai konpartitua liberatuko du (seinale bat bidaliz erlojuari).
		 */
		pthread_cond_signal(&mutex.cond);
		/**
		 * @brief cond2 aldagai konpartitua liberatu arte itxaroten geratuko da.
		 * Mutex aldagaia liberatuko du.
		 */
		pthread_cond_wait(&mutex.cond2, &mutex.mutex);
	}
}

/**
 * @brief Prozesuak sortzeko tenporizadorea.
 * 
 * @param maiztasuna
 * @return void* 
 */
void* timerCreateProcess(void * input){
	int kont=0;
	int maiztasuna=*((int *) input);
	/**
	 * @brief Tenporizadore bat sortzean, tenporizadore kopurua handiagotuko da. (+1)
	 */
	mutex.tenp_kop++;
	/**
	* @brief Mutex aldagaia blokeatuko du beste hariei sarbidea ukatuz.
	*/
	pthread_mutex_lock(&mutex.mutex);
	while(1){
		/**
		 * @brief done aldagai konpartitua +1, honek adieraziko du tenporizadore batek 
		 * behar zuena exekutatu duela.
		 */
		mutex.done++;
		if (kont==maiztasuna){
			printf("Kaixo ni prozesuen tenporizadore bat naiz.\n");
			int processPrio= rand()%150; //1etik 150era izango dira gure lehentasunak. (ausaz aukeratuko da)
			int processDenbora= (rand()%(quantum))+1;//1etik quantum*10era izango da prozesu bakoitzaren denbora (ausaz)
			printf("Prozesuaren lehentasuna (random): %d\n",processPrio);
			printf("Prozesuaren denbora (random): %d\n",processDenbora);
			createProcess(processPrio, processDenbora);
			kont=0;
		}
		kont++;
		/**
		 * @brief cond aldagai konpartitua liberatuko du (seinale bat bidaliz erlojuari).
		 */
		pthread_cond_signal(&mutex.cond);
		/**
		 * @brief cond2 aldagai konpartitua liberatu arte itxaroten geratuko da.
		 * Mutex aldagaia liberatuko du.
		 */
		pthread_cond_wait(&mutex.cond2, &mutex.mutex);
	}
}