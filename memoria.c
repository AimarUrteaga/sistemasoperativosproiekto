//TODO nirar los int si tienen que ser long porque igual no da para indexar en memoria
#include "memory.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>

struct hitza* memoria;
unsigned int berretzailea;//tamaño de la memoria
unsigned int azkenErreserba;//azken erreserba
struct nodoHutsuneLista* hutsuneListaPage;//Page Tableko hutsune lista
struct nodoHutsuneLista* hutsuneListaMemoria;//Beste memoriako hutsune lista
//struct tlbData* tlb;//para hacer el tlb la mas eficiente seria una doble linked list

void hasieratuMemoria(){//TODO comprobar todas las matematicas
	unsigned int hitzKop=pow(2,berretzailea);//senbat hitz dauden memorian
	memoria=malloc(sizeof(struct hitza)*hitzKop);//memoria eresbatzen da
	unsigned int numFrame=hitzKop/pow(2,BERRETZAILEHITZFRAMEKO);//senbat frame dauden
	azkenErreserba=hitzKop/numFrame;//aske erreserbatutako hitza
	hutsuneListaPage=malloc(sizeof(struct nodoHutsuneLista));//utzune lista asieratzen du
	hutsuneListaPage->hasierakoHelbidea=0;
	hutsuneListaPage->tamaina=azkenErreserba;
	hutsuneListaPage->hurrengoa=NULL;

	hutsuneListaMemoria=malloc(sizeof(struct nodoHutsuneLista));//utzune lista asieratzen du
	hutsuneListaMemoria->hasierakoHelbidea=azkenErreserba+1;
	hutsuneListaMemoria->tamaina=hitzKop-azkenErreserba;
	hutsuneListaMemoria->hurrengoa=NULL;
	/*struct nodo* tmpmomento=malloc(sizeof(struct nodo));
	tlb=tmpmomento;
	tlb->markoZenbakia=UINT_MAX;
	tlb->orriZenbakia=UINT_MAX;
	for (i=1;i<TLBTAMAINA;i++){
		tmpmomento->hurrengoa=malloc(sizeof(struct nodo));
		tmpmomento=tmpmomento->hurrengoa;
		tmpmomento=tmpmomento->markoZenbakia=UINT_MAX;
		tmpmomento=tmpmomento->orriZenbakia=UINT_MAX;
	}
	tmpmomento->hurrengoa=NULL;*/
}

void hasieratuTLB(struct tlbData* tlb){
	int i;
	for (i=0;i<TLBTAMAINA;i++){
		tlb[i].markoZenbakia=UINT_MAX;
		tlb[i].orriZenbakia=UINT_MAX;
	}
}

unsigned int markoaTLB(struct tlbData* tlb, unsigned int orriZenbakia){
	int i;
	for (i=0;i<TLBTAMAINA;i++){
		if (tlb[i].orriZenbakia==orriZenbakia){
			int j;
			unsigned int lagmarkoZenbakia=tlb[i].markoZenbakia;
			unsigned int lagorriZenbakia=tlb[i].orriZenbakia;
			for (j=i;j>0;j--){
				tlb[j].orriZenbakia=tlb[j-1].orriZenbakia;
				tlb[j].markoZenbakia=tlb[j-1].markoZenbakia;
			}
			tlb[0].orriZenbakia=lagorriZenbakia;
			tlb[0].markoZenbakia=lagmarkoZenbakia;
			return(lagmarkoZenbakia);
		}
	}
	return(UINT_MAX);
}

unsigned int lortuHelbideFisikoa(struct tlbData* tlb, unsigned int ptbr, unsigned int helbideBirtuala){
	unsigned int berretzaileDesp=berretzailea-BERRETZAILEHITZFRAMEKO;
	unsigned int orriZenbakia=helbideBirtuala>>berretzaileDesp;//orri senbakia kalkulatzen dut
	unsigned int markoZenbakia=markoaTLB(tlb, orriZenbakia);
	if (markoZenbakia==UINT_MAX){// mro si esta en el tlb
		memcpy(&markoZenbakia,&memoria[ptbr+orriZenbakia],sizeof(struct hitza));// si no esta miro en la memoria
		int i;
		for (i=TLBTAMAINA;i>0;i--){//lo añado en el tlb
			tlb[i].orriZenbakia=tlb[i-1].orriZenbakia;
			tlb[i].markoZenbakia=tlb[i-1].markoZenbakia;
		}
		tlb[0].orriZenbakia=orriZenbakia;
		tlb[0].markoZenbakia=markoZenbakia;
	}
	markoZenbakia=markoZenbakia<<berretzaileDesp;//bien en el tlb bien en la memoria se guarda sin el desplazamiento hecho por lo que hay que desplazarlo
	unsigned int maskara=(unsigned int)pow(2,BERRETZAILEHITZFRAMEKO)-1;
	unsigned int desp= maskara & helbideBirtuala;//desplasamendua kalkulatzen dut
	return(markoZenbakia&desp);
}

struct hitza memoriaBirtualetikDatua (struct tlbData* tlb, unsigned int ptbr,unsigned int helbideBirtuala){
	return (memoria[lortuHelbideFisikoa(tlb, ptbr,helbideBirtuala)]);
}

void memoriaBirtualeraDatua ( struct hitza sarrera,struct tlbData* tlb, unsigned int ptbr,unsigned int helbideBirtuala){
	memoria[lortuHelbideFisikoa(tlb, ptbr,helbideBirtuala)]=sarrera;
	return;
}

int pageTableEraldatu(unsigned long pageTableHasiera, unsigned long int frameKopurua){
	struct nodoHutsuneLista* tmp=hutsuneListaMemoria;
	unsigned long int alokatutakoFrameKopurua=0;
	while (tmp!=NULL)
	{
		alokatutakoFrameKopurua+=tmp->tamaina;
		if (alokatutakoFrameKopurua>=frameKopurua){
			struct nodoHutsuneLista* tmp2=hutsuneListaMemoria;
			struct nodoHutsuneLista* eliminar;
			int i;
			unsigned int helbidea;
			alokatutakoFrameKopurua=0;
			while (tmp2!=tmp)
			{
				for (i=0;i<tmp2->tamaina;i++){
					helbidea=tmp2->hasierakoHelbidea+i;
					memcpy(&memoria[pageTableHasiera+alokatutakoFrameKopurua].hitza,&helbidea,sizeof(unsigned int));
					alokatutakoFrameKopurua++;
				}
				eliminar=tmp2;
				tmp2=tmp2->hurrengoa;
				free(eliminar);
			}
			for (i=0;i<frameKopurua-alokatutakoFrameKopurua;i++){
				helbidea=tmp2->hasierakoHelbidea+i;
				memcpy(&memoria[pageTableHasiera+alokatutakoFrameKopurua+i].hitza,&helbidea,sizeof(unsigned int));
			}
			tmp2->tamaina-=i;
			if (tmp2->tamaina==0){
				tmp=tmp->hurrengoa;
				free(tmp2);
			}
			else{
				tmp2->hasierakoHelbidea+=i;
			}
			hutsuneListaMemoria=tmp;
			return 0;
		}
		tmp=tmp->hurrengoa;
	}
	return -1;
}

unsigned long int memoriaErreserbatu(unsigned int textHitzKopurua, unsigned int dataHitzKopurua,unsigned long* frameLuzera){
	unsigned long frameKopurua=(textHitzKopurua+pow(2,BERRETZAILEHITZFRAMEKO)-1)/pow(2,BERRETZAILEHITZFRAMEKO)+(dataHitzKopurua+pow(2,BERRETZAILEHITZFRAMEKO)-1)/pow(2,BERRETZAILEHITZFRAMEKO);
	*frameLuzera=frameKopurua;
	struct nodoHutsuneLista* tmp=hutsuneListaPage;
	unsigned int zenbatEresbatuak;
	unsigned long buelta;
	if (tmp->tamaina==frameKopurua){
		buelta=tmp->hasierakoHelbidea;
		if (-1==pageTableEraldatu(buelta, frameKopurua)){
			return ULONG_MAX;
		}
		hutsuneListaPage=tmp->hurrengoa;
		free(tmp);
		return buelta;
	}
	if (tmp->tamaina>frameKopurua){
		buelta=tmp->hurrengoa->hasierakoHelbidea;
		if (-1==pageTableEraldatu(buelta, frameKopurua)){
			return ULONG_MAX;
		}
		tmp->tamaina-=frameKopurua;
		tmp->hasierakoHelbidea+=frameKopurua;
		return buelta;
	}
	while (tmp->hurrengoa!=NULL){
		if (tmp->hurrengoa->tamaina==frameKopurua){
			buelta=tmp->hurrengoa->hasierakoHelbidea;
			if (-1==pageTableEraldatu(buelta, frameKopurua)){
				return ULONG_MAX;
			}
			struct nodoHutsuneLista* borrar=tmp->hurrengoa;
			tmp->hurrengoa=tmp->hurrengoa->hurrengoa;
			free(borrar);
			return buelta;
		}
		if (tmp->hurrengoa->tamaina>frameKopurua){
			buelta=tmp->hurrengoa->hasierakoHelbidea;
			if (-1==pageTableEraldatu(buelta, frameKopurua)){
				return ULONG_MAX;
			}
			tmp->hurrengoa->tamaina-=frameKopurua;
			tmp->hurrengoa->hasierakoHelbidea+=frameKopurua;
			return buelta;
		}
		tmp=tmp->hurrengoa;
	}
	return ULONG_MAX;
}