#include "memory.h"
#ifndef __CORE_H_
#define __CORE_H_

#include "globals.h"

struct core {
	unsigned int PC;
    unsigned int IR;
	unsigned int PTBR;
    struct tlbData tlb;
};

void hasieratuCoreGuztiak();

#endif