/**
 * @file globals.h
 * @author Mikel Aristu & Aimar Urteaga
 * @brief Aldagai eta egitura konpartituak deklaratzeko header fitxategia.
 * @version 0.1
 * @date 2021-11-16
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef __GLOBALS_H_
#define __GLOBALS_H_

#include <pthread.h>
#include "scheduler.h"
#include "processgenerator.h"
#include "clock.h"
#include "memory.h"
#include "core.h"

/**
 * @brief aldagai konpartituen defektuzko balioak.
 */
#define NSCHEDULER 1 //coreProzesadoreko 
#define PRIORIDADES 150 //lehentasun Kopurua
#define CUANTUM 100 //quantum-a
#define TSCHEDULER 10 //Schedulerraren denbora

/**
 * @brief aldagai konpartituak (hauek izango dira parametro bidez aldatu ditzazkegunak)
 */
extern int yscheduler;          //core prozesadoreko
extern int lehentasunak;        //lehentasun kopurua
extern int quantum;             //quantum-a
extern int schedulerMaiztasuna; //schedulerraren denbora
extern struct hitza* memoria;   //representa la memoria
extern int berretzailea;        //representa en el tamñao de la memoria en exponenciales de 2

extern struct pidbitmap pidavailable;
extern struct o1scheduler* o1;

extern struct core* nukleoak;

/**
 * @brief mutex aldagai konpartitua eta globala, clock.c eta 
 * timercreate.c programetan erabili ahal izateko.
 */
extern struct clk_mutex_datu mutex;
#endif