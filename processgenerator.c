/**
 * @file processgenerator.c
 * @author Aimar & Mikel
 * @brief Prozesuak sortuko dituen programa.
 * @version 0.2
 * @date 2021-11-27
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "processgenerator.h"
#include "scheduler.h"
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include "globals.h"
#include <stdio.h>
#include <string.h>
#include <math.h>


struct pidbitmap pidavailable;







unsigned int hexToDecimal (char * hex,int luzera){
    int i;
    unsigned base=1,decimal=0;
    for(i = luzera-1; i >= 0; i--){
        if(hex[i] >= '0' && hex[i] <= '9'){
            decimal += (hex[i] - 48) * base;
            base *= 16;
        }
        else if(hex[i] >= 'A' && hex[i] <= 'F'){
            decimal += (hex[i] - 55) * base;
            base *= 16;
        }
        else{
            return UINT_MAX;
        }
    }
    return decimal;
}

 struct programa* leerELF(char * nombreArchivo){
    FILE * archivo = fopen(nombreArchivo, "r");
    if (archivo == NULL){
        return NULL;
    }
    struct programa* momentu=malloc(sizeof(struct programa));
    if (fread(&momentu->lerroak,sizeof(struct hasiera),2,archivo)==0){
        return NULL;
    }
    momentu->nLista=0;
    momentu->bestelerroak=malloc(sizeof(struct nodoPrograma));
    //momentu->bestelerroak->hurrengoa=NULL;
    char tmp[9];
    struct nodoPrograma* nodoa=momentu->bestelerroak;
    while (fread(&tmp,sizeof(char)*9,1,archivo)!=0){
        nodoa->zenbakia=hexToDecimal(tmp,8);
        nodoa->hurrengoa=malloc(sizeof(struct nodoPrograma));
        nodoa=nodoa->hurrengoa;
        momentu->nLista++;
    }
    free(nodoa);
    return momentu;
}









void inicializarProcessGenerator(){
    unsigned int i;
    #pragma omp parallel for private(i) shared(pidavailable)
    for (i=0; i<UINT_MAX-1; i++){
        pidavailable.ocupados[i]=0;//marco todos los pid como libre
    }
    pidavailable.pos=0;//pongo el ultimo pid a 0
    pidavailable.lastsscheduler=0;//elijo el scheduler de manera rotatoria//en un futuro luego el scheduler podra cambiarse los procesos
    pidavailable.lastProgramNumber=0;
}

unsigned int buscarlibre(){//busca el uptimo pid libre de manera rotatoria
    unsigned int inicial=pidavailable.pos;
    while(pidavailable.pos<UINT_MAX-1){//miro del principio al final el ultimo disponible
        if (!pidavailable.ocupados[pidavailable.pos]){
            return pidavailable.pos;// si lo encuentro lo mando
        }
        pidavailable.pos++;
    }
    pidavailable.pos=0;//restablezco la posicion
    while(pidavailable.pos<inicial){//ciclo el resto de la lista
        if (!pidavailable.ocupados[pidavailable.pos]){
            return pidavailable.pos;//si lo encuento lo mando
        }
        pidavailable.pos++;
    }
    printf("Ez dago pid librerik\n");//sino imprimo esto que es chungo
    return UINT_MAX;//mando el codigo de error
}

void createProcess(unsigned int prioridad, unsigned int tiempo){
    unsigned int pid=buscarlibre();//busco un pid
    if (pid==UINT_MAX){//miro si hay pid disponible
        return;
    }
    int nDigits = floor(log10(abs(pidavailable.lastProgramNumber))) + 1;

    char* fileName="prog";
    int i;
    for (i=nDigits;i<=3;i++){
        strcat(fileName,"0");
    }
    char numero[4];
    sprintf(numero, "%d", pidavailable.lastProgramNumber);
    strcat(fileName,numero);
    strcat(fileName,".elf");
    struct programa* kargatuPrograma =leerELF(fileName);
    if (kargatuPrograma=NULL){
        return;
    }
    pidavailable.lastProgramNumber++;
    unsigned long ptbr = memoriaErreserbatu(kargatuPrograma->nLista,0,NULL);
    if (ptbr==ULONG_MAX){
        return;
    }

    struct hitza sarrera;
    struct nodoPrograma* tmp = kargatuPrograma->bestelerroak;
     struct nodoPrograma* eliminar;
    for (i=0;i<kargatuPrograma->nLista;i++){
        memcpy(sarrera.hitza,&tmp->zenbakia,sizeof(unsigned int));
        memoriaBirtualeraDatua(sarrera,&nukleoak[yscheduler].tlb,ptbr,i);
        eliminar=tmp;
        tmp=tmp->hurrengoa;
        free(eliminar);
    }





    struct nodo * insertar=malloc(sizeof(struct nodo));//resebo memeoria para el nodo
    insertar->proceso=malloc(sizeof(struct pcb));//resebo memeoria para el proceso de dentro
    insertar->proceso->tiempo=tiempo;// le pongo tiempo
    insertar->proceso->pid=pid;//le agrego el pid
    insertar->proceso->PC=0;
    insertar->proceso->IR=0;
    pidavailable.ocupados[insertar->proceso->pid]=1;//marco el pid como utilizado


    insertar->proceso->memoryData.code=(unsigned long) atoi(kargatuPrograma->lerroak[0].edukia);
    insertar->proceso->memoryData.data=(unsigned long) atoi(kargatuPrograma->lerroak[1].edukia);
    insertar->proceso->memoryData.pgd=ptbr;
    free(kargatuPrograma);


    insertar->next=o1[pidavailable.lastsscheduler].expirado[prioridad];//inserto el proceso en el scheduler
    o1[pidavailable.lastsscheduler].expirado[prioridad]=insertar;
    pidavailable.lastsscheduler++;
    if (pidavailable.lastsscheduler==yscheduler){//reseteo el scheduler en el que lo tengo que meter//TODO arreglar
        pidavailable.lastsscheduler=0;
    }

}







