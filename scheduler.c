/**
 * @file scheduler.c
 * @author Aimar & Mikel
 * @brief Kernelaren planifikatzailearen programa.
 * @version 0.2
 * @date 2021-11-25
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "scheduler.h"
#include "processgenerator.h"
#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "globals.h"
extern struct pidbitmap pidavailable;

struct o1scheduler* o1;

void inicializarSchedulers(){
	o1=malloc(sizeof(struct o1scheduler)*yscheduler);
	int i,j;
	for (i=0;i<yscheduler;i++){//inicilizo cada scheduler
		o1[i].quantumDenbora=0;//pongo el quantum a 0
		o1[i].encual=0;//pongo el que se esta ejecutando ha 0
		o1[i].ejecutando=malloc(sizeof(struct nodo)*lehentasunak);//reservo memoria para cada lista
		o1[i].expirado=malloc(sizeof(struct nodo)*lehentasunak);
   		//#pragma omp parallel for private(j) shared(o1,i)
		for (j=0;j<lehentasunak;j++){//inicilizo todas las listas
			o1[i].ejecutando[j]=NULL;
			o1[i].expirado[j]=NULL;
		}
	}
}

void scheduler(struct o1scheduler* entrada){
	if (entrada->ejecutando[entrada->encual]!=NULL){
		struct nodo* tmp;
		entrada->ejecutando[entrada->encual]->proceso->tiempo-=schedulerMaiztasuna;//quito el tiempo
		if (entrada->ejecutando[entrada->encual]->proceso->tiempo<=0){//miro si el proceso se ha acabado ya
				tmp=entrada->ejecutando[entrada->encual];//elimino el proceso
				entrada->ejecutando[entrada->encual]=entrada->ejecutando[entrada->encual]->next;//elimino el proceso
				pidavailable.ocupados[tmp->proceso->pid]=0;
				printf("%u prozesua amaitu da.\n",tmp->proceso->pid);
				free(tmp->proceso);//elimino el proceso
				free(tmp);//elimino el nodo
				entrada->quantumDenbora=0;//reseteo el tiempo de ejecución
		}else{
			entrada->quantumDenbora+=schedulerMaiztasuna;//añado al contador del quantum
			if(entrada->quantumDenbora<quantum){
				return;
			}
			tmp= entrada->ejecutando[entrada->encual]->next;//se le agota el quantum y lo paso a la lista para la siquiente ejecución
			entrada->ejecutando[entrada->encual]->next=entrada->expirado[entrada->encual];
			entrada->expirado[entrada->encual]=entrada->ejecutando[entrada->encual];
			entrada->ejecutando[entrada->encual]=tmp;
			entrada->quantumDenbora=0;//reseteo el tiempo
			printf("%u prozesuaren quantuma agortu da.\n",entrada->expirado[entrada->encual]->proceso->pid);
		}
	}
	//unsigned int inicioentrada=entrada->encual;
	
	while (entrada->encual<lehentasunak){//busco proceso
		if (entrada->ejecutando[entrada->encual]!=NULL){//paso ha ejecucion
			return;
		}
		entrada->encual++;
	}
	struct nodo** tmp2;
	entrada->encual=0;//como ya me he recorrido toda la lista cambio las posiciones
	tmp2=entrada->ejecutando;
	entrada->ejecutando=entrada->expirado;
	entrada->expirado=tmp2;//doy buelta a las listas
	printf("Listei buelta eman zaie.\n");
	while (entrada->encual<lehentasunak){//paso ha ejecucion
		if (entrada->ejecutando[entrada->encual]!=NULL){//paso ha ejecucion
			return;
		}
		entrada->encual++;
	}
}


/*void scheduler(struct o1scheduler* entrada){
	unsigned int inicioentrada=entrada->encual;
	while (entrada->encual<lehentasunak){
		if (entrada->ejecutando[entrada->encual]!=NULL){//busco proceso para ejecutar
			struct nodo* tmp;
			if (entrada->ejecutando[entrada->encual]->proceso->tiempo<=0){//miro si el proceso se ha acabado ya
				tmp=entrada->ejecutando[entrada->encual];//elimino el proceso
				entrada->ejecutando[entrada->encual]=entrada->ejecutando[entrada->encual]->next;//elimino el proceso
				pidavailable.ocupados[tmp->proceso->pid]=0;
				printf("%u procesua amaitu da\n",tmp->proceso->pid);
				free(tmp->proceso);//elimino el proceso
				free(tmp);//elimino el nodo
				entrada->quantumDenbora=0;//reseteo el tiempo de ejecución
				scheduler(entrada);//como se ha acabado este coloco otro
			}else{
				entrada->quantumDenbora+=schedulerMaiztasuna;//añado al contador del quantum
				if(entrada->quantumDenbora>=quantum){//se le agota el quantum y lo paso a la lista para la siquiente ejecución
					tmp= entrada->ejecutando[entrada->encual]->next;
					entrada->ejecutando[entrada->encual]->next=entrada->expirado[entrada->encual];
					entrada->expirado[entrada->encual]=entrada->ejecutando[entrada->encual];
					entrada->ejecutando[entrada->encual]=tmp;
					entrada->quantumDenbora=0;//reseteo el tiempo
					printf("%u procesuaren quantuma agortu da\n",entrada->expirado[entrada->encual]->proceso->pid);
					scheduler(entrada);//coloco otro
				}
			}
			//printf("Exekusiaorekin jarraitu da\n");
			return;
		}
		entrada->encual++;
	}
	entrada->encual=0;//como ya me he recorrido toda la lista cambio las posiciones
	struct nodo** tmp=entrada->ejecutando;
	entrada->ejecutando=entrada->expirado;
	entrada->expirado=tmp;//doy buelta a las listas
	//printf("Listei buelta eman saie\n");
	if (inicioentrada!=0){
		printf("Lista ez dago agortuta\n");
		scheduler(entrada);
	}
	return;
}
*/

void schedulers(){
	int i;
	for (i=0;i<yscheduler;i++){
		scheduler(&o1[i]);//elijo lo que hay que ejecutar en el scheduler especifico
	}
}


/*void main (){
	printf("hau froga bat da\n");
	inicializarprocessGenerator();
	inicializarSchedulers();
	createProcess(0,800);
	createProcess(0,700);
	createProcess(148,100);
	int i;
	for (i=0;i<1000;i++){
		schedulers();
	}
}*/