/**
 * @file main.c
 * @author Mikel Aristu & Aimar Urteaga
 * @brief main funtzioa, hemendik deituko ditugu programatutako funtzioak.
 * @version 0.5
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <getopt.h>
#include <limits.h>
#include "globals.h"
#include "timerCreate.h"

int yscheduler;
int lehentasunak;
int quantum;
int schedulerMaiztasuna;
int berHitzFrameko;
int tlbTamainaa;

/**
 * @brief Parametro bezala pasatutakoa zenbakia den aztertzeko.
 * @note Funtzio honek ez du aztertzen positiboa baldin bada, hori gero aztertu beharko da.
 * @param zenbakia: Parametro bezala sartutako elementu bat.
 * @return 0 zenbakia bada (ondo joan da), 1 ez bada zenbaki positiboa (errorea), 2 ez bada zenbakia (errorea),
 * 3 zenbaki handiegia bada.
 */
int zenbakiaDa(char zenbakia[]){
    int i,length = 0;
    /**
     * @brief Zenbakia ez da positiboa.
     */
    if (zenbakia[0] == '-'){
		return 1;
		i=1;
	}
	/**
	 * @brief atoi funtzioa mugatuta dagoenez, stroll funtzioarekin eta zenbakia long long motara pasatuz, zenbaki handiegiak
	 * ekidin ditzazkegu.
	 * @library stdlib.h liburutegia strtoll funtzioa erabiltzeko.
	 */
	long long zbk = strtoll(zenbakia, NULL, 10);
	if (zbk>=INT_MAX){
		return 3;
	}
	length=strlen(zenbakia);
	/**
	 * @brief Bektorearen digitu guztiak aztertu banan-banan. (isdigit funtzioa erabili ahal izateko)
	 */
    for (; i<length; i++){
        /**
         * @brief Bektorearen digituren bat ez baldin bada zenbaki bat, bueltatu 2.
         * @library ctype.h libuturegia
         */
        if (!isdigit(zenbakia[i])){
            return 2;
		}
    }
    return 0;
}

/**
 * @brief Hautazko parametroak jasotzeko eta behar diren balioak kudeatzeko.
 * 
 * @param argc Programa osoa exekutatzerakoan pasatzen diren argumentu kopurua.
 * @param argv Programa osoa exekutatzerakoan pasatzen diren argumentuen bektorea.
 * @param processMaiztasuna 
 */
void parametroakJaso(int argc, char *argv[], int *processMaiztasuna){
	int opt;
	/**
	 * @brief Parametrorik gabe deitu da.
	 */
	if(argc==1){
		printf("Defektuzko aukerak aukeratu dituzu.\n");
	/**
	* @brief Bakarrik parametro batekin deitu da.
	*/
	}else if (argc==3){
		printf("Hau izan da zure aukera:\n");
	/**
	 * @brief Parametro bat baino gehiagorekin deitu da.
	 */
	}else{
		printf("Hauek izan dira zure aukerak:\n");
	}
	
	/**
	 * @brief Parametro bakoitzarentzat errore aldagai eta ondo dagoen aldagai bat (positiboa dela ziurtatzeko) deklaratu,
	 * ondoren hauek erabiliko dira errore-kontrola egiteko.
	 */
	int cZbkE;
	int cZbkO;
	int lZbkE;
	int lZbkO;
	int qZbkE;
	int qZbkO;
	int sZbkE;
	int sZbkO;
	int pZbkE;
	int pZbkO;
	/**
	 * @brief Pasatutako parametro guztiak jaso getopt funtzioaren bitartez, parametro bakoitzari dagokion aukera exekutatuz.
	 */
    while((opt = getopt(argc, argv, ":c:l:q:s:p:")) != -1) 
    { 
        switch(opt) 
        { 
            case 'c': 
				/**
				 * @brief Ziurtatu ea jarritako elementua zenbaki bat den.
				 */
				cZbkE = zenbakiaDa(optarg);
				cZbkO = atoi(optarg);
				/**
				 * @brief atoi funtzioa, jasotako parametroak string moduan direnez, int motara pasatzen ditu. 
				 * @library stdlib.h liburutegia atoi funtzioa erabiltzeko eta stdio.h liburutegia errore estandarrean idazteko.
				 */
				if(cZbkE==2){
					fprintf(stderr, "%s", "Sartutako core prozesadoreko ez da zenbakia!!!\n");
					exit(1);
				}else if(cZbkE==1){
					fprintf(stderr, "%s", "Sartutako core prozesadoreko positiboa izan behar da!!!\n");
					exit(1);
				}else if(cZbkE==3){
					fprintf(stderr, "%s", "Sartutako core prozesadoreko zenbakia handiegia da!!!\n");
					exit(1);
				}else if(cZbkO==0){
					fprintf(stderr, "%s", "Sartutako core prozesadoreko 0 baino handiagoa izan behar da!!!\n");
					exit(1);
				}else{
					//Ondo joan da.
                	printf("	- Core Prozesadoreko: %s\n", optarg);
				}
				yscheduler=cZbkO;
                break; 
            case 'l':
				lZbkE = zenbakiaDa(optarg);
				lZbkO = atoi(optarg);
				if(lZbkE==2){
					fprintf(stderr, "%s", "Sartutako lehentasun kopurua ez da zenbakia!!!\n");
					exit(1);
				}else if(lZbkE==1){
					fprintf(stderr, "%s", "Sartutako lehentasun kopurua positiboa izan behar da!!!\n");
					exit(1);
				}else if(lZbkE==3){
					fprintf(stderr, "%s", "Sartutako lehentasun kopurua zenbakia handiegia da.!!!\n");
					exit(1);
				}else if(lZbkO==0){
					fprintf(stderr, "%s", "Sartutako lehentasun kopurua 0 baino handiagoa izan behar da!!!\n");
					exit(1);
				}else{
                	printf("	- Lehentasun Kopurua: %s\n", optarg); 
				}
				lehentasunak=lZbkO;
                break;
			case 'q':
				qZbkE = zenbakiaDa(optarg);
				qZbkO = atoi(optarg);
				if(qZbkE==2){
					fprintf(stderr, "%s", "Sartutako quantuma ez da zenbakia!!!\n");
					printf("Sartutako quantuma ez da zenbakia!!!\n");
					exit(1);
				}else if(qZbkE==1){
					fprintf(stderr, "%s", "Sartutako quantuma positiboa izan behar da!!!\n");
					exit(1);
				}else if(qZbkE==3){
					fprintf(stderr, "%s", "Sartutako quantuma zenbakia handiegia da.!!!\n");
					exit(1);
				}else if(qZbkO==0){
					fprintf(stderr, "%s", "Sartutako quantuma 0 baino handiagoa izan behar da!!!\n");
					exit(1);
				}else{
                	printf("	- Quantuma: %s\n", optarg);
				}
				quantum=qZbkO; 
                break;
			case 's':
				sZbkE = zenbakiaDa(optarg);
				sZbkO = atoi(optarg);
				if(sZbkE==2){
					fprintf(stderr, "%s", "Sartutako schedulerraren denbora ez da zenbakia!!!\n");
					exit(1);
				}else if(sZbkE==1){
					fprintf(stderr, "%s", "Sartutako schedulerraren denbora positiboa izan behar da!!!\n");
					exit(1);
				}else if(sZbkE==3){
					fprintf(stderr, "%s", "Sartutako schedulerraren denbora handiegia da!!!\n");
					exit(1);
				}else if(sZbkO==0){
					fprintf(stderr, "%s", "Sartutako schedulerraren denbora 0 baino handiagoa izan behar da!!!\n");
					exit(1);
				}else{
                	printf("	- Schedulerraren denbora: %s\n", optarg); 
				}
				schedulerMaiztasuna=sZbkO;
                break;
			case 'p':
				pZbkE = zenbakiaDa(optarg);
				pZbkO = atoi(optarg);
				if(pZbkE==2){
					fprintf(stderr, "%s", "Sartutako processGenerator-aren maiztasuna ez da zenbakia!!!\n");
					exit(1);
				}else if(pZbkE==1){
					fprintf(stderr, "%s", "Sartutako processGenerator-aren maiztasuna positiboa izan behar da!!!\n");
					exit(1);
				}else if(pZbkE==3){
					fprintf(stderr, "%s", "Sartutako processGenerator-aren maiztasuna handiegia da!!!\n");
					exit(1);
				}else if(pZbkO==0){
					fprintf(stderr, "%s", "Sartutako processGenerator-aren maiztasuna 0 baino handiagoa izan behar da!!!\n");
					exit(1);
				}else{
					printf("	- ProcessGeneratoraren maiztasuna: %s\n", optarg);
				}
				*processMaiztasuna=pZbkO;
				break;
            case '?':
                printf("Aukera ezezaguna: %c\n", optopt);
                break; 
        } 
    }
}

/**
 * @brief main funtzioa
 * 
 * @param argc Programa honi deitutako parametro kopurua. (Argument Counter)
 * @param argv Programa honi deitutako parametro array-a. (Argument Vector)
 * @return int Dena ondo joan bada, 0 itzuli, bestela 1.
 */
int main(int argc, char *argv[]){
	//Defektuzko balioak esleitu.
	yscheduler=NSCHEDULER;
	lehentasunak= PRIORIDADES;
	quantum=CUANTUM;
	schedulerMaiztasuna= TSCHEDULER;
	int processMaiztasun=25;

	berHitzFrameko=BERRETZAILEHITZFRAMEKO;
	tlbTamainaa=TLBTAMAINA;

	//Hautazko parametroak kudeatu.
	parametroakJaso(argc, argv, &processMaiztasun);

	/**
	 * @brief Conditional Mutexaren done eta tenp_konp 0z hasieratu.
	 */
	mutex.done = 0;
	mutex.tenp_kop = 0;
	/**
	 * @brief Conditional Mutexaren mutex aldagaia hasieratu.
	 */
	pthread_mutex_init(&mutex.mutex, NULL);
	/**
	 * @brief Conditional Mutexaren cond eta cond2 baldintzak hasieratu.
	 */
	pthread_cond_init(&mutex.cond, 0);
	pthread_cond_init(&mutex.cond2, 0);

	/**
	 * @brief Hasieratu memoria.
	 */
	hasieratuMemoria();
	
	/**
	 * @brief Core guztiak hasieratu.
	 */
	hasieratuCoreGuztiak();

	/**
	 * @brief Process Generator eta Scheduler-ak hasieratu.
	 */
	inicializarProcessGenerator();
	inicializarSchedulers();

	
	int i;
	pthread_t id[3];
	/**
	 * @brief Hari bat sortu erlojua exekutatzeko.
	 */
	pthread_create(&id[0], NULL, clk, NULL);
	/**
	 * @brief Tenporizadore bakoitzarentzat hari bat sortu exekutatzeko.
	 */
	pthread_create(&id[1], NULL, timerCreateSchedulers,NULL);
	pthread_create(&id[2], NULL, timerCreateProcess, (void*) &processMaiztasun);
	
	/**
	 * @brief Hari guztiei itxaron hurrengo iterazioarekin hasteko.
	 */
	for(i=0; i<3; i++){
		pthread_join(id[i], NULL);
	}
	
}

