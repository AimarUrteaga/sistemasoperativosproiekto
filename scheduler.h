/**
 * @file scheduler.h
 * @author Aimar Urteaga & Mikel Aristu
 * @brief Planifikatzailearen header fitxategia.
 * @version 0.1
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef __SCHEDULER_H_
#define __SCHEDULER_H_

struct mm
{
	unsigned long data;
	unsigned long code;
	unsigned long pgd;
};


/**
 * @brief Prozesuen egitura orokorra.
 */
struct pcb {
	unsigned int pid;
	int tiempo;
	unsigned int PC;
	unsigned int IR;
	struct mm memoryData;
};

/**
 * @brief Expired eta Active ilaretako nodoen egitura.
 */
struct nodo {
	struct pcb* proceso;
	struct nodo* next;
 };

/**
 * @brief O1 planifikatzailearen egitura.
 */
struct o1scheduler {
	unsigned int quantumDenbora;//tiempo que lleva ejecutado la cacharra actual
	unsigned int encual;
	struct nodo** expirado;
	struct nodo** ejecutando;
};

void inicializarSchedulers();

void schedulers();
#endif