/**
 * @file clock.h
 * @author Mikel Aristu & Aimar Urteaga
 * @brief Erlojuaren header fitxategia.
 * @version 0.1
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef __CLOCK_H_
#define __CLOCK_H_

#include <stdio.h>

/**
 * @brief Sistemaren clock-a sortzeko eta martxan jartzeko
 * funtzioaren deklarazioa.
 */
void* clk();

/**
 * @brief Conditional mutex baten egitura orokorra
 * @mutex Conditional mutex aldagaia (lock-unlock egiteko).
 * @cond eta cond2  Conditional mutexaren baldintza bat tenporizadoreak 
 * eta erlojua sinkronizatzeko.
 */
struct clk_mutex_datu{
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	pthread_cond_t cond2;
	int done;
	int tenp_kop;
};
#endif
