/**
 * @file memory.h
 * @author Aimar Urteaga & Mikel Aristu
 * @brief Memoriari buruzko header fitxategia.
 * @version 0.1
 * @date 2022-01-18
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef __MEMORY_H_
#define __MEMORY_H_
#define BERRETZAILEHITZFRAMEKO 2
#define TLBTAMAINA 5

struct hitza
{
	char hitza[4];
};

struct tlbData
{
	unsigned int markoZenbakia;
	unsigned int orriZenbakia;
};

struct nodoHutsuneLista
{
	struct nodoHutsuneLista* hurrengoa;
	unsigned long hasierakoHelbidea;
	unsigned long tamaina;
};

void hasieratuMemoria();
void hasieratuTLB(struct tlbData* tlb);
struct hitza memoriaBirtualetikDatua (struct tlbData* tlb, unsigned int ptbr,unsigned int helbideBirtuala);
unsigned long int memoriaErreserbatu(unsigned int textHitzKopurua, unsigned int dataHitzKopurua,unsigned long* frameLuzera);
void memoriaBirtualeraDatua ( struct hitza sarrera,struct tlbData* tlb, unsigned int ptbr,unsigned int helbideBirtuala);

#endif